/**
 * 
 */
package br.com.massuda.alexander.spring.framework.infra.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Alex
 *
 */
@Configuration
@ComponentScan(basePackages = "br.com.massuda.alexander.spring.framework.infra")
public class Configuracao {

}
