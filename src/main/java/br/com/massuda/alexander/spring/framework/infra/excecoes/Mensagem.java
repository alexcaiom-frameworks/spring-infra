package br.com.massuda.alexander.spring.framework.infra.excecoes;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Alex
 * Classe Mãe de Erros/Exceções
 */
public class Mensagem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1727546945514929736L;
	public String codigo;
	public String mensagem;
	
	public Mensagem() {
	}
	public Mensagem(String descricaoErro) {
		this.mensagem = descricaoErro;
	} 
	
	public Mensagem(Throwable e, String descricaoErro) {
		this.mensagem = descricaoErro;
		if (e != null && !StringUtils.isNotEmpty(e.getMessage()) && StringUtils.isEmpty(this.mensagem)) {
			this.mensagem = e.getMessage();
		}
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	@Override
	public String toString() {
		return "Erro [mensagem=" + mensagem + "]";
	}
	
}