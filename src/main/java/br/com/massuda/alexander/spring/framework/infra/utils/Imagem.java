/**
 * 
 */
package br.com.massuda.alexander.spring.framework.infra.utils;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.stream.Stream;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

/**
 * @author Alex
 *
 */
@Component
public class Imagem {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String base64 = "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgMCAgMDAwMEAwMEBQgFBQQEBQoHBwYIDAoMDAsKCwsNDhIQDQ4RDgsLEBYQERMUFRUVDA8XGBYUGBIUFRT/2wBDAQMEBAUEBQkFBQkUDQsNFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBT/wAARCACbAm8DASIAAhEBAxEB/8QAHgABAAEEAwEBAAAAAAAAAAAAAAIBBwgJBAUGAwr/xABYEAABAgQDAwUJCggMBAcBAAAAAQIDBAURBgcSITFRCEFhcZETFBgiUoGUodEJMkJWV2KSscHSFRkjM3OTsuEWFyQ0U2NkcnSCwvA2OEOiJTVFRnWDhLP/xAAbAQEAAQUBAAAAAAAAAAAAAAAABAECAwUGB//EACwRAQACAQMDAwMDBQEAAAAAAAABAgMEERIFITETMlEGFEEiUoEVM0JhcSP/2gAMAwEAAhEDEQA/ANqYBHUBQmR0kgBHSSI6gJECZAAAAAXalgAPNYhwTJ4pn4EWpapiXg7WSyrZmrivE5cHCVLl4fc4dPlms4JCb7DudCXuVI84Mdp3tG6yaRPlb/EmUdFrcFIkCC2nzjNsOPLMRFY7jbnOvoGSNJlVdMVd7qtPv9++MtmfR9qqXN7khVGo3daxG+yw2ty2W+jj+HkJjK7DkzC7m6kyzW/1bEZ9R8cJ4HiYLnI0GQmYsSkR11pLxvGWA/i1eHRY9tpKIyy3513mWulxUnlWNpVjHFfCTAE2AmMgAVtsAoAApuKtkOHUJ6HT5aJHjPRkOG3U5y8yHJiOsiFpc+8TuplHhUyFE0xZxV12XajE9q2QgazURpsM5GLNf08fJaPHeMI+MK/Fmnvcksx3c4ENUsjW+086LJq1bVXmuu4HkWfNbPk52ndy1rzknlYABGVAAFQABbvAACm64u5HIrXaURtrJ9ZdXI/G76bUvwHORP5PMO1QL7dL/JLVH1lZuLJzMOPLu0xGP1o7nRTYaHUW02ojJE9l+HLNL7s02u1bt5M6HCNcbX8PSM+1yL3eC12zmVd/Yd6i3PXsN4vSLx+XUVneN4VABIXAAA6PGLXvwrWEhXSL3lHRipv1aNhpdwlyjsyssq1GiUTFlSgLCiqiy01E7tCWy2VNL7m7iLBR7HMf4zHb0XnS1rGk/lUZWxspM8cQ0d7VZJR48Sck3LufBiOu1U6ucDN/k1+6H0rMKblqBj6XgYfrsRNMGowFVsnML866/k3dG3rM0oMw2MxHNejmrtRUW6KnE/Pnp8dHb3It0Vfr6+k2Qe538qGcxXDXLfFM2+Yn5aEr6VNzC+NGhN2vhuXnVE2ot9vADPEFGu1FQAAAAAAAAAtcAD56LcfNckrbldvEbeJSd5N/9Kf75hpTghIFTsjpTpKLDTh61J+cect2k/hBGIv7lUk1ukafONRbvv4g7R4VBTUFciF+/bdXZWw28D4RpqHLsc+I9GMbvc5bIh4Su51Yeob3Q0mlnIjd6S6ak7SLl1OLB/ctsx3vWkb2lcG/QVLWU/P6hTcbTHhzMjD/AKWLDu3/ALbqXBptZlKvLNmZOO2PBd8JjkVE69pjw6vFn9llK5K38S7JNoKMcVJ7IAAAAAK6hpKEwAI6iQAjpJAAQJkAAAAAAAAAAAAAAAAAC7CKu4Klue5VyqWwzbzJjYTlYUnIPYlRjortTkvpbxtzkLU6iulxzktKy96443suDHq8pKutFmIUF3kveiL2H1gT0KZbqgxGRW+UxyKhhvO1GbqMd0aZmYsaIvwnPW52WGsY1TCs8yPJzLlhp76BEVVa45an1Fjm+1q9mu++jdl09bpdFMd82qDiKuYtmpplMmYslCRrIMRrbt0JtVfO7buL3YVxJAxPQZapQEtCjMV2ly+9VOYtDW88KtScQVKUZJycxKwI6w2akcjlai233t6jYdRy4MuCvOdq28M2pml6RynaJWtmKRPSn5+TmIKcYkJyfYcN70h++2daKn2F5pbP+XiNtN0Rf/qiavracp2aWCKsn8upjof6aXT7DkfsNPf2ZGqjT4p8WWQaup1ksvUqD/e1LfXYvav8V1bS6pKwF6lhlf4rsEVNl5OrRIP6KbRf2kUpPSv2XiVPs/iVkk29P91U9pQvJM8n6FFZeUrir+lgo76lOmnchq3AusCblJhE+crPrQi36Vnp7Y3/AJU+3yfC2gPYzmUuKJNL/g1YzeMGI131KdLNYQrkk60akzbE49yVSLbR6ivmksE4ckeYdQCcSDGg/nIMSH/eYqEEW/Onb7SNOK9fNZOM/ALW3bAl/JXsVfsCuRHWVUROKqhZ+qGKYmPK/wDyfKqszhuakVW7pOOrUReZrt3ZZewu23gY/wDJ3nXMrNUltaLDiQWxFROKKif6l7S/91PV+kZfU0tZl0+ltzxQkADfJQV0kVcmy3jX3W3nl8bZm4Zy6kFnMR1uSpEDm75io17upu9QPTuTVzmNPLU5MTM/8EQpylJDZi2kIsSTdayx4S++gqvNfmdttwU6/EXujuT9EjvhS09UqwrfhyUm7SvndY4VJ90sykqEwkOadWaW3nizEojktw8VVA1ZVqiT2HapM02pysWRn5Z2iNLxmK17XeTbj6uk7jLDGc1l7mFQsRSMTucenTkKPqvsVjNlupU2KbO8fYAyI5ZMqk1K12TZXtNoNRp0VsGbv89FtrTo2L0mNeLfcwcfUydd+AK5SKxKotmJFVZaK1vzmqqp2OUDZpRalCrFKk5+DbuMzAZGZZb7HJex2Gk8xgKkzGHcF0WlzenviRkmQYulyKl2ttfYedx/yhsvsr2O/hFiiRkojd8FsRHxE/ypt9QFydI0mK8f3SPJyFH0Q52qzDPLh06J7D2GCOWzlDjyabKyOK4EpNO3Qp9joCr53JYC/GkocKUqcvUJaHMyseFMy8RNUONBej2vToVN69CHNvcAAAAAAAAAAAAAXYBDX4t77eB5vFOPKTg+XbFqExpc/wB5CZte/qQ5eJq3Cw9RZufje8l4SxXWXfwTrVdhYqjZe17M+ovrFQiLJy0Z2tIkRt3NTg1t00ml1upy4/0Ya72R8uS9Z4443l6Sd5RkPU7vWjxYjE3OiRNN/MjVOdQuUBT6hMQ4NQko0hr3RdXdIfnWyL6jsqbkLhyVY3vmHHn3pvdGiWv9GxWoZDYZmmL3ODGlYi/9SDEVFNZWnUd+c2/hG46j5eIzXxvO4irEKg0Z748JNKPbLbXRHLzdCJxI4XyDm6gxkarzfeaLvgQUu53WvMXFwJldJYLixo2t05MvVUbHjN8ZrV5r329Z7pjVRNl9vPzmSnTvXt6uq7z8K009rzzyT/C2kHIHDcJu102+J/SLGs71IhzKDli/B882LRqjHSUd+elJuz2v6l2aS4mno9RTRfeqL5jb49Fgx+2uyXGOlfEIwlVeax9BZAbJlAAAAAFdI1EiAAmR0kgAAAECZAAAAAAAAAAAAAAAAACMR1mmLmc02s1j+eaq6mQmQoTU8nZdxlBGdaGq9BiNjmb79xpWYu9qzT0Rb83vU9px/wBQ34YIpv5a3XW2rs6IAHnERPlz8eV/8iokSHgWae9bwoc1EWHq5moiL9qp5iw1QjrNT0xGdviPV6+dbl+sG2omSyx1XQroEaJfrc5U9VkMftPvrrvOm6nM1wYaT8btlqZ2x0r+VV27/G6x2p1KAc1/xr6xMeZFS7r3VU4OspJHW5rpwVVIgvi9q+Jldvb5cyXrE9KfmJyPBT5kZyfadxK5jYkk9KQ6xMqic0RUff6SKebBJjVZqeLLvUyfL30lndieV/OxpacThGgIn7NjvJblC1CElpuky8Vf6qKrfrRS0oTZ09ZIr1PUV/ylnjUZa/leyHnfQJz+f0SIzqYyJ7CbsYZc1n+dSUKH+lgafqUsgiW3bOpE9hVXLzeL1EiOrZI/uUiy/wC6uvamFcs6wn8nmoMv+jmHM+tSETJHDs420jXY7V491ZE+xCyiNRPgt7CcONFg/m4r4X6NyoZP6jhv7sWy6M+OfNGReXGVzsEVqZm/wi2aZFg9zRnc9LkW6LfevD1lyb83PwMUcMZkV3DEZFhTSzUuq3fAj+M13Uu9DIjA2M5TGdIScgfk3N2RGuW6sXgdf0rW6bLWMWOOMtlgz1v+mr1RRztJVNpRzdR06cw/5YXLegZNRY+FMIrL1LGTmJ3eM7xodPRd2q2xz15m3TpVDWZjDG9fx/WI1UxFV5qsT0XfFm4iu0/3U3IZzctXkP6W1PMPAqR5iK6IszVKS5yxFW++LC23uvO23UqGv1Wvbp1MVL229fMnTx4AFVXJZbdV1t61UNuxbtcrfmotm9iAAfWVm5inzLZiUjxJSO3dFgvVjk86WL24A5a2b+XcuktJYpfUZRrdLIFVhpMtZ1Ku3tVSxoAvrmBy2M3cxpV8pO4nfTpNyW73pUJssn0m+N6yx05MRqhMOjzMV8xGdviRXK96/wCZbr6z5gCPc2qllu7rt7CTruRvjO8XciqqonbdfWABeLIjlV47yJqzFpdQfUKO938ppM+9YkCM3ovthr0tt1G1/InPrDefWEIddoEVzVb4s1JRlRI0s/yXJwXmXn4IaPbX37VLqcm/PGpZB5nSVdlIr302K6HAqksqqrY8F29V6W8y22AbvkS6FDrKBXZXElEkqrT4rY8nOQWx4L0XY5rkunsOzAAAAAAAAABdoAHTYgw/L4klIctN3WXSK2K9jdmvTtRF6L7TspeVZAYjWNRrU3NTYiH3Bh9OOXPbupxjfcbdOcOS6gGTaFTr2hqIgA2gAAXAAAAAAAACZHSNQ1ASAAAjqJEdIEiBMgAAAAAAAAAAAAAAAABxZ6OkCTixHe9Y1yqvUYaTcdZiajxV9897n363XMsceTiyWEqtFTe2XdbbzqhiO1tmtS97c/E8/wDqTJHKlGl6hb2wqUculrlVN3NxKn2lICzM3Lwt6xHoztdY4/HE2vFY/LU0jedoX6xU51FyWhQNOh3esKE5F5lW1/qXtMfy/Oe0x3lgyTlU/wCpHYipuujSwxuusWn1Ix/FU3V+6sAAOehFAAXAAAAAAAAAAABcjIuvupmKosi9+mHOQ1RGru7ontLbnNo1TfQ6vITrXaXwIjYjndKb+02Ghzfb6it4nszYb8LbszWOKnCp082dk4UeHZYcRjXoqLfYpzb7D1+totWLQ6aJ3jdxo0BsRPyjEfdLOS2xU4Gpfl88n+BkzmfCrNHg9xw5iRIkzBY1fEgTCOTu0Nqc12qjkXrS2y5txciKYpe6O4Yh1vk4zlQe1qxaTPS81Aem9iK5Ia+p638xmVanAAAAAAAAAAAKqt78yKitsnDmKADbB7nNmS/GmRaUqbjd2nKBMrJ7d/c3WdD6rXVPMZYGtz3KesRW4wxzSNf8niyUGb0fPbE0ovY/1GyMAAUc7SBUHR4lxfTsKSffNQjtgw1XS267VU8B4Q9E7v3PvOc0+XZtvrIeTV4cVuNrd2KctaeZXbC2Q8tQsfUjEklEmZKaY9kJLvRzrK3r6Ok8TWuULTpOYdBp0lFqGnfFR6NZ5l2mC2tw0rym3ZS2alfMrvXRd1ipaSgcoCnVKbbAnpR0hq3Pc9FTz3RC6kCabMQmxGWVrk1It9ioScOpx5/ZK6uStvD7gAlMgAAAAAAAAAAAAAAACukaRqGoBqJEdI1ASAI6gJECZAAAAAAAAAAAAAAAAFNQHgc6pzvXAc6iLZYqIxNvExjL/coSc7nhqTgItu6zG1L8zSwJ5f8AUFuWqiJ/DQa+d77B32BJTv8AxhSIS7nTCPXZeyI650J7bJuTdN46kltshw3PXntY1OiiMmppWEXBEc4h63lDzF4lHlr7kdFVOpWonbdews4XGz5nFmMZshIuyDKw0tfn1PcvqsW5JHVL76qy/Uzvl7AANQwAAAAAAAAAAAAArspHcKKxN3Mu9F5yoKeO6rI/I+vLVMJtl4sTXFlHrDVFXboX3v29hcvV0GOeRNc/B+Io8k99mTTNifOTd2eN2mRbfGPWOkaj7jTRv5h0emyepRIxb90Zr8OjcmmpSr3sbEqM9LSsJi/Ds/WqeZsN3qMoHvVEvp9e/q6TVx7o9nrAx9jum4MpMw2YpWH1c+ZiQ11NiTLtKW/yNRUvdb6l3G9SmHQAAAAAAAAAAAADN/3KiWc/MvG8ZE2Q6PDh34KsZLfsmzEwB9ykw86DS8fVt7PFix5WUY+2/Sx73bf87TP4AfOKqpzXPoLXAt3OZYS+I61GqddiOm3Kvc4Mq11ocJv2r2HbNyyw2sHuf4IldP6ND1ncm3vay9BVEsQp0mG08rVY/Tr+VpqlkbJRJpsSlzsenQYqaJqFDW6RGcE2pZenaetoWXNCoEBkOBToLnN3RIjEc/tses0pwTbvKmOmhw0tyiq2MVY8vPVLBNHq8u+DN0+BGa7feG2/bY+2G8PMw5JJJQI8SLLsX8mkZdSsbwvzndaSqJZbkquKtPbGy+KVjwAAzrwAAAAAAAAAAAAAAAAFdJQCuoaRpJACBMjpAkQJkAAAAAAAAAAAAAAAQJlNIFjuUaj/APway/ktb0Xhq5kLLGU+ZGDWYzw86V2NmmokSC9ybnJ9RjPWaHPUCYiQJ6WfLvZvV7VsvUqHmXW9JljUWyRG8S0OupbnycAupyfJHu2I6hMWs2FLo1F32Vylt6XSJ2tR2wJGWiTUZfgw2rbtUyQyywN/AagLDjK1Z2OqxIytT6KeYxdI0mT1ozTXtCmkxTa8XmO0LH5qTnfmP6u+2xsTuKJfdphIl+1F7TyZ2OJZrv7EdUmP6WZe+1728a/1bDrjTay/qZ7WhGyzvk3gABFYgAAAAAAAAAAD7S0pFm4nc4TNb7Ktk6G3Pie6yapbKli17XtuxsqrlRdu/wAUz4MM6jLGODHE2njDw1vequxHblKHY4gpLqHWpuQe3Q6A9zbXvZE3L5zriy9Jx2mk+YVmNp4y7LDdTfRa9JTkN+lIMe69XAy5lZ5sWXbGRU7mqXRVXenHha225hnbZbm5+k9fndTK3mTyTqo3D1Rm5Wr0yH3ZzJOKrVjsh++Y+21UVnMdh9Paja1sTY6C+1uDynLD5b0hgWlTmEsCzsOfxLHh6JifhLqgyEPoX4UTqNZcxHizkxFjx4r40eK5znxHuu5yu33U+b36nanreKq38e+x/OvX13B6C3YAAAAAAAAAAA4XRUVdqbObiD22TGW03m1mfh/C8miv/CEwiRHLtRkG13uXgjURetbJsuBtM5BOAHYD5OtCWLB7lMVV0SoxbpZbOdaHfj+TRvYZHHXUOjy9BpElTZOGkKUk4EOXhMTcjGJZE7DsQAAAAAAAAAAAAAAAAAAAAAAAAAAAAACuooV0jSBIAACOokR0gSIEyAAAAAAAAAAAAAAAAAEFYiovPficKYpcCaskaXZFRN2tEX6zsBt4mK+OmT3RupMRPmHXylLlZG3cJeHCtztaiKfKuzXedInY6WasOE56KnQ252mk6nEEm6dos5Lt3xYDmonC6WIuWnDFaKdlsxEV2rDDxzlfEe9Vur7385QrEY+FFiQ3Ns9i6XNXejuBQ8ezRMZZiXK27TsAAxKT2AAPBvAAAqAAAAABdzk7yfd6tVZlU2Q4DIabPKc5f9JaMvtydZJUo9Vm0X85NIxEtzNbe/8A328xvOjY+erql6OItl7vMZ80D8HYll6gxulk5DRj1ts1Iu7zlsDLHHOEIOMqJHko1taLqhPtdWLxMcq7l5XqBMPhx6fFisT3sSAmprif1fQZKZrXx17Sy6rBaMnKI7PNl6uT5qjyVclojUdCdFhudDc3YupFR/q2FuaLl3iCvRmMgU6LCY7fEjpoahkPl9gWDgqkugNd3aPFW8aL5a9XMV6Ho80Zudo2hdo8V4vymGpflm5GRMjs3JuHLMczD1YdEnadE+Cjb2fDXpau7im3YWHN1fKeyDp/KAy0nKHHa1lWl1WZps1v7lGRLW6lTennNM+J8N1HB+IJ+jVWVfKz8jFdCjwnpZWqnP1dJ6S3brAAAAAAAAACKPRUcvMm5V5wJJZyrZb22rxtzbDZR7mzkK7DmG5vMiry6tqNXRZanNiJ7yWuiq7oV6onUl023MSOSVycp3lB5jQpWYguhYZprmx6pN2VLonvYSL5Tufh0m4+jUaVodLladIwGS0nLQ2woUKGlmsa3dZAOwaiIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACuokR0kgI6iRHSSAAAAQJkAAAAAAAAAAXYlwQiREZCc5yo1G3VVVdyJvUDg1euSVBkI07UZmDISkFLvjzMRIcNvSrl3IWMrnLxyXoNRfJxMVtm4rPfuk4LojE/zWQ1/8sHlMVfO/H1RpknOxIGDKVMPl5OVhOVrJlzXW7tE8pV4bjHe12K1bW6NgG4D8YFkn8Zo3oEb7o/GBZJ/GaN6BG+6af9KcE7BpTgnYBuC8P/JP4zxvQI33R+MByT+M8f0CN900/W6E7CuzyU7ANv34wLJP4zRvQI33SC8v7JVU/wCJo/oEb7pqC0pwTsHZ2Fs1i3kbG8weUnk1WalFqVFxVoiRl1RIDpGMiOdxvp2dh5ynZ44Fqn5nEkoz9MqsMB9KXvsv1BG8dvmQ5rVdCwaieURtKBl0dMlt2x6SxPR6izVK1WSmE/q5hi/adm270u1NbfmeN9RrQhufBfqY9zF+YtkO1kMXV2lreUrE7Lu+ZMPt61NVf6a/bdFt0+PxLYyrtN1cmlqb1d4v12GpFW3wuhbp6rmB9Nz7zApdkhYnm4rW7kmGsi/tIp6incrLHMolphKZPN/rJXT+y5CDb6fz19s7sP2N2ZK7OPYoMXqfyzJpn5OoYWhRPnScy5v1tU9TTuV9hmP/ADylVGS6ka/7UNbk6NqsfirD9vk+F+AWupvKWwBUv/V3S3+Igub9h6qm5n4Tq/8ANcQ0+J1xrfWRL6LPT3VY5x2jzD04PjLT0rOW73moExf+iitX7TkOY5G6tLlb5WlbEWcV6+ays4z8IOcjb7d2+56fBPK7ypyvpkWgV3EXedVlY7++YbJd7ka66La6b9iWLLZsZv0vLSjR7TEKarURmmXk4So97el/Mhg3P1GYqk/MzszEc+amHuiRIiLtVy8+252f0/or1yereNobTR4pieUw26eH/kmv/uWPfikhG+6R8PnJTVf+EsZejvCNb9k1B9nYNCdHYd9xj/Lu3H/YbfE5fOSiLdMTRurvCNb9kknugGSaItsTRkVedKfG+6agdCdHYNKcE7Cya/t7Db74f2SVlT+E0dUVVWyyEbn/AMpiryxcZ5HZ8yzMQ4axI2TxnLtWE7+QxmQ56GvwXrp2L86y9RhZpTgnYV+CqbNvRsLxzPwN/bpP9b+4fgb+3Sf639xwrdCdiC3QnYgHN/A39uk/1v7j0FPwNR5qBrmcaUiRf5D4cd/7LFPJW6E7EKp4q3RGp/lRPqsB3dUwxJycVrZLEVMqjV+FAc9v7TUOB+Bv7dJ/rf3HEVyuVupdSJzLt+sjboTsQDm/gb+3Sf639xzqDhqQnq3Jy9UrklTZB77RplVWJ3JvlaW7+q6HSW6E7ECNRL22JusiImzhssBtFyf5TvJ3yTwVJYbw9iGNDloLUWLGfT42uYi/CiPXTtVfV0nu090DyTRP+Jo/oEb7pp/0J0dg0pwTsA3AfjAsk/jNG9AjfdJfjAck/jPH9AjfdNPulOCdhLZ5KdgG378YFkn8Zo3oEb7o/GBZJ/GaN6BG+6af9KcE7BpTgnYBuA/GBZJ/GaN6BG+6V8P7JT4zRvQI33TT9pTgnYTv0N7ANvsT3QLJSFD1LiWOvQkhG+6euy+5V+VuZ86ySoGK5SNPP95KTF4ER/8AdR6Ii9ppRRLbredCTHuhRWRYa6IrPeRGq7Uz+6t7oB+gRkfVdLeNv03225l859jCf3PPlM1HMuQncCYlnXzlapcBJiRm4i/lY8si6Va5edWqrdvPfmM2E2gAAAAAAAAAAAAAAAAAABMAAR1EiOkkAAAAgTIAAAAAAAAADzmPIU1MYJxE2Uuk66nTKQERdqRFgrpTtPRnziJdFRbKi22fX6gPz8LqSIrHKqvR+1Fv76+rb07HIqcyoDKvlp8k2s5XYzqmKMPyMafwbU5h8yqwm61kIz11PbEttRivVyo5E2ItrLvMU0ejtWlUVG73cyfb6gKgpfq+khTureLfpJ7QJAqll2X26rb07d5RFvt3e+t5gAAAAXVHKjk0q3eiqiLzfaq9gVeFndTk9pSZ2jcAS0L/ALRSll4L2KUi1Z/IoCuleC9ikO6JdyJzdKe0rExPiVN48JKl+Cr0lEavOqL1ohTureLfpJ7SbPyi2bdzudqJtQtmJtO0yb/6UW6/CVOpVI9zTgidWz6ieleC9ii3R6lLP/P87ETSfMPvLVKck9Pe83Hl7f0UVyfads3H2Jkl+4LiCorD4d8uv9Z0VvNx2KRv1fSQsnT4reYhZtT4fSYjRZuI6JHivjRHe+dEcrtXXzkBqTym/SQXTykM1YisbVjZk/5ABdPKQrdPKb2l4oBdPKQXTykAp5ipK6cW9v7il08pvaBQDUnlN+kgunlIAAunlINTePrT2gANTePrT2jU3j609oADU3j609o1J5TfpIAA1J5TfpIR7q3i36Se0CQKNejm6k2t8pNqFQABRHtVurUiNTe5Vsicd4FQR1p/tU9o1p/tU9oEgUc7S2+ztT2n3pklNVqdgydPlYs/NxvzUGVRIjonUiLftQDJD3OyFNLypKJ3vr7jDkZ10zb+jczZfoWJoNuqbDELkF8lydyWok7inEUFsLFNZhNhMgqnjS0sjtSsd85Vtw3GXoAAAAAAAAAAAAAAAAAAAV1DUUAFdRIgTAEdRIgBMgTIAAAAAAAAACjm6ioA+EeVhTMN8OIxj2PSzmuaio5OCou9C2VZ5LeU2IJ9ZyoZf0GYmHb395MYq/RRELqAC0XgkZOfJ1h/0No8EfJv5OcP+htLugCzc9yTcnoUpMPbl3QUckNy370bzGmGqMbCqU/DY3SxseKjU4WdY38VP+YTP6J5oIrH/nFR/wARG/bA4oAA2m8kbk35ZYz5OWCqxW8E0ap1OblnOjzMzKNc56pGf7ELpVbki5Qvps0yBl5QYcVYXiK2Ub4qnG5D/wDyq4A/wj//AOzy+Mf3vmMOT2SNesTJDA0J7muwnS7tVyKncE2WPn/EjgT4q0v0dC5uMWNg4qrMNiaWNmH2anMdSeRZ9TqK5piLuYy5L1ntLxf8SuA/inS/1CGSuEOTDk9XMN0+cfl3QHLHgNVypKN2qu/sLPGS2R8R0TAMpqcrtLnol+ZE3HRdA1WW+W0XndN0F72t3l0q8knJyyKmXGH/AENpaPO/ITKigpJU6nYCoUvMRdUWI9ksmpGpu7bO7DLhfeGLOcExEi48ntb1doWGxt+Zq709a9pvetam+HTfo8pmsyTjx7wsz/ErgP4p0v8AUIU/iTwD8UqX+oQ9qDzj73U/uaD1L/LrcA8nLAOJsVSUjEwhTHQNbokVEgJ71Ny+cyPbyScnVal8ucPp/wDjaed5PEvDfVanGViLFbCho1y70RS/6bj0jolr20/qXneW+0lZ9PlMrS+CVk38nOH/AENpTwScnPk4oHoqe0u4Do09aPwScnPk4oHoqe0eCPk18nFA9FQu4ALR+CTk58nFA9FT2jwScnPk4oHoqe0u4ALR+CPk18nFA9FQeCPk18nFA9FQu4ALSeCVk38nOH/Q2lPBJyc+Tigeip7S7gAtH4JOTnycUD0VPaV8EnJz5OqD6I0u2ALSeCTk58nVB9EaPBJyc+Tqg+iNLtgC0ngk5OfJ1QfRGjwSsm/k5w/6G0u2ALSeCVk38nOH/Q2kfBHyb+TnD/obS7oA0n8q/DFLwdyhMZUiiyUKm02VmNEGWl00tb4qL/qLSF7uWr/zQY8/xf8AoYWRAGQnIRwPQcxeUHLUjEtJla1TX0uaiOl5xmpqvbaztipxMezKD3Nz/mfkv/iJz/QBsO8EfJv5OaD6I0eCPk38nNB9EaXdAFoV5IuTSpb+Lmg+iNPT4MyVwNl7FSLhzClKpEVP+tLSrEiL1vVFd6z25MD4Nho1Nlr32rbav7z6AAAAAAAAAAAAAAAAAAAAB//Z";
		Imagem decoder = new Imagem();

//		decoder.converterParaArquivo(base64, "c:/base64/decoderimage.jpg", "/public_ftp/api-usuario/images/decoderimage.jpg");
		decoder.converterParaArquivo(base64, "c:/base64/api-usuario/alexcaiom/2020-11-01_21-07-21.jpg", "/public_ftp/api-usuario/images/decoderimage.jpg");
	}

	public void converterParaArquivo(String base64, String local, String nome) {
		FTPClient client = new FTPClient();
		try {
			File arquivo = getArquivo(base64, local);
			InputStream is = new FileInputStream(arquivo);
			
			ftpConnect(client);
			if (diretorioInexistente(nome, client)) {
				criarCaminhoArquivo(client, nome);
			}
			boolean sucesso = client.storeFile(nome, is);
			System.out.println(client.getReplyString());
			fechar(client, is);
			if (sucesso) {
                System.out.println("The first file is uploaded successfully.");
            }
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		verifyExistingFile(nome);

	}
	
	public String get(String local, String nome) {
		String retorno = "";
		FTPClient client = new FTPClient();
		try {
			ftpConnect(client);
			
			File arquivo = verifyExistingFile(local);
			OutputStream os = new FileOutputStream(arquivo);
			
			boolean sucesso = client.retrieveFile(nome, os);
			System.out.println(client.getReplyString());
			fechar(client, os);
			if (sucesso) {
				System.out.println("The first file is uploaded successfully.");
			}
			
			retorno = loadLocal(local);
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		verifyExistingFile(nome);
		return retorno;
	}

	private void fechar(FTPClient client, Closeable is) throws IOException {
		if (ObjectUtils.isEmpty(client) && ObjectUtils.isEmpty(is)) {
			client.logout();
			is.close();
			client.disconnect();
		}
	}

	private boolean diretorioInexistente(String nome, FTPClient client) throws IOException {
		return ArrayUtils.isEmpty(client.listDirectories(nome));
	}

	private void ftpConnect(FTPClient client) throws UnknownHostException, SocketException, IOException {
		InetAddress hostname = InetAddress.getByName("alexcaiom.com.br");
		client.connect(hostname);
		String username = "alexcaio";
		String password = "lifE2sg@3ood";
		client.login(username, password);
		client.enterLocalPassiveMode();
		client.setFileType(FTP.BINARY_FILE_TYPE);
	}

	private File getArquivo(String base64, String local) {
		File arquivo = verifyExistingFile(local);
		saveLocal(base64, local);
		return arquivo;
	}

	private void criarCaminhoArquivo(FTPClient client, String pathname) throws IOException {
		System.out.println("Working directory: "+client.printWorkingDirectory());
		for (int pathIndex = 1; pathIndex < pathname.split("/").length-1; pathIndex++) {
			String tempPathName = getSimplePathName(pathIndex, pathname);
			if (diretorioInexistente(tempPathName, client)) {
				System.out.println("Pasta " +tempPathName+ " criada com sucessso: "+client.makeDirectory(tempPathName));
				client.changeWorkingDirectory(tempPathName);
				System.out.println(client.getReplyString());
			}
		}
	}

	private String getSimplePathName(int pathIndex, String nome) {
		String[] paths = nome.split("/");
		int possibleFileNamePosition = paths.length-1;
		if (!ObjectUtils.isEmpty(paths) && pathIndex!= possibleFileNamePosition) {
			StringBuilder sb = new StringBuilder();
			for (int i = 1; i <= pathIndex; i++) {
				sb.append("/").append(paths[i]);
			}
			return sb.toString();
		}
		return null;
	}

	private void saveLocal(String base64, String nome) {
		try (FileOutputStream imageOutFile = new FileOutputStream(nome)) {
			byte[] decode = Base64.getDecoder().decode(base64);
			imageOutFile.write(decode);
		} catch (FileNotFoundException e) {
			System.out.println("Image not found" + e);
		} catch (IOException ioe) {
			System.out.println("Exception while reading the Image " + ioe);
		}
	}
	
	private String loadLocal(String nome) throws IOException {
//		StringBuilder contentBuilder = new StringBuilder();
		 
//        try (Stream<String> stream = Files.lines( Paths.get(nome), StandardCharsets.UTF_8)) 
//        {
//            stream.forEach(s -> contentBuilder.append(s).append("\n"));
//        }
//        catch (IOException e) 
//        {
//            e.printStackTrace();
//        }
//		return Base64.getEncoder().encodeToString(contentBuilder.toString().getBytes());
		return Base64.getEncoder().encodeToString(Files.readAllBytes( Paths.get(nome) ));
	}

	private File verifyExistingServerFile(String nome) {
		File arquivo = new File(nome);
		if (!arquivo.exists()) {
			try {
				String[] paths = nome.split("/");
				String pathToFile = null; 
				StringBuilder sb = new StringBuilder();
				boolean primeira = true;
				for (int i = 0; i < paths.length-1; i++) {
					if (!primeira) {
						sb.append("/");
					}
					sb.append(paths[i]);
					primeira = false;
				}
				pathToFile = sb.toString();
				arquivo = new File(pathToFile);
				if (!arquivo.exists()) {
					arquivo.createNewFile();
				}

				arquivo = new File(nome);
				arquivo.createNewFile();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			arquivo = new File(nome);
		}
		return arquivo;
	}

	private File verifyExistingFile(String nome) {
		File arquivo = new File(nome);
		if (!arquivo.exists()) {
			try {
				String[] paths = nome.split("/");
				String pathToFile = null; 
				StringBuilder sb = new StringBuilder();
				boolean primeira = true;
				for (int i = 0; i < paths.length-1; i++) {
					if (!primeira) {
						sb.append("/");
					}
					sb.append(paths[i]);
					primeira = false;
					pathToFile = sb.toString();
					arquivo = new File(pathToFile);
					if (!arquivo.exists()) {
						arquivo.mkdirs();
					}
				}

				arquivo = new File(nome);
				arquivo.createNewFile();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		return arquivo;
	}

}
